# Paiza-SteinsGate-RankingProblem

paiza × STEINS;GATE コラボ 【電脳言語のオルダーソンループ】   
[ランキング問題 ネット・ガーディアンの奮闘](https://paiza.jp/steins_gate/ranking) 自作Visualizer

https://neetsdkasu.gitlab.io/paiza-steinsgate-rankingproblem/  


※paizaのテストケースの分布とは異なるので注意  　　

 - H = 30, W = 30 固定  
 - 1 <= テスト番号 <= 30 は 正の値のセルの割合 10% 固定 セルの値はなるべく重複しないよう設定  
 - 31 <= テスト番号 <= 60 は 正の値のセルの割合 11～40% セルの値はランダム  
 - 61 <= テスト番号 <= 65 は 市松模様 (行番号+列番号)が奇数のセルが正の値 セルの値はランダム  
 - 66 <= テスト番号 <= 70 は 3の倍数の行と3の倍数の列の交点のセルのみ正の値 セルの値は重複しないよう設定  

-------------

testcase000.txt ... 現行の我がソルバーがちょっと苦手とするパターンを手入力したもの
```
24 16
  15 -999 -999 -999 -999 -999 -999 -999 -999 -999   10 -300    1 -999 -999 -999
 -15 -999   -2   -2  100   -5  100   -1   -1 -999  -10 -999   -2   15   15   15
  -1 -999   -1 -999   -3 -999   -3 -999   -3 -999   -1 -999   -1   15   -1   15
  -1   -1  100   -1   -2   -1   -2   -1  100   -1   -1 -999   -1   15   15   15
  -1 -999 -999 -999 -999 -999 -999 -999 -999 -999   -1 -999   -1 -999 -999 -999
  -1 -999 -999 -999 -999 -999 -999 -999 -999 -999   -1 -999   -1   15   15 -999
  -1 -999 -999 -999 -999 -999   -1   -2   -2 -999   -1 -999   -1   15   -1   -1
  -1 -999 -999   -1   -1   -1   -1 -999   -1 -999   -1 -999   -1   15   -1   -1
  -1   -1   -1  100 -999 -999  100 -999  100   -1   -1 -999   -1   15   15   15
  -1 -999 -999   -1   -1   -1   -1 -999   -1 -999   -1 -999   -1 -999 -999 -999
  -1 -999 -999 -999 -999 -999   -2   -1   -1 -999   -1 -999   -1   15   15 -999
  -1 -999 -999 -999 -999 -999 -999 -999 -999 -999   -1 -999   -1   15   -1 -999
  -2 -999 -999 -999 -999 -999 -999 -999 -999 -999   -1 -999   -1   15   15 -999
  -1 -999 -999 -999 -999 -999   -1   -2   -2 -999   -1 -999   -1 -999 -999 -999
  -1 -999 -999   -1   -4   -1   -1 -999   -1 -999   -1 -999   -1   15   15   15
  -1   -1   -1  100 -999 -999  100 -999  100   -1   -1 -999   -1   -1   -1   -1
  -1 -999 -999   -1   -1   -1   -4 -999   -1 -999   -1 -999   -1   15   15   15
  -1 -999 -999 -999 -999 -999   -2   -1   -1 -999   -1 -301   -1 -999 -999 -999
  -1 -999 -999 -999 -999 -999 -999 -999 -999 -999   -1 -400   -1   15   15 -999
  -1 -999 -999 -999 -999 -999 -999 -999 -999 -999   -1 -300   -1   15   -1   -1
  -1   -1   -1   -1  100   -3   -5   -2  100   -1   -1 -301   -1   15   15 -999
  -1 -999 -999 -999   -2 -999 -999 -999   -2 -999   -1 -301   -1 -999 -999    1
   0 -999 -999 -999   -2   -2    3   -3   -1 -999   -1 -999   -1   15   15   -2
  -2    1 -999 -999 -999 -999 -999 -999 -999 -999    1 -300   -1   -1   -1   -1
```
'use strict';

const CELL_SIZE = 12;
const DEFAULT_TESTCASE_COUNT = 70;
const testCases = new Array(DEFAULT_TESTCASE_COUNT);

function shuffle(rand, arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const k = rand.nextInt(i + 1);
        const tmp = arr[k];
        arr[k] = arr[i];
        arr[i] = tmp;
    }
}

class TestCase {
    constructor(rand, p_min, p_max, gentype) {
        const H_MAX = 30;
        const H_MIN = 30;
        const W_MAX = 30;
        const W_MIN = 30;
        const P_MAX = 10;
        const P_MIN = 10;

        p_min = p_min ?? P_MIN;
        p_max = p_max ?? P_MAX;
        gentype = gentype ?? 0;

        this.H = rand.nextInt(H_MAX - H_MIN + 1) + H_MIN;
        this.W = rand.nextInt(W_MAX - W_MIN + 1) + W_MIN;

        if (gentype === 0) {
            let scores = new Array(this.H * this.W);
            let ps = new Array(1000);
            let ms = new Array(1000);
            for (let i = 0; i < ps.length; i++) {
                ps[i] = i;
                ms[i] = -i;
            }
            shuffle(rand, ps);
            shuffle(rand, ps);
            shuffle(rand, ms);
            shuffle(rand, ms);
            this.plus = rand.nextInt(p_max - p_min + 1) + p_min;
            this.plus = Math.floor(this.plus * scores.length / 100);
            for (let i = 0; i < scores.length; i++) {
                if (i < this.plus) {
                    scores[i] = ps[i];
                } else {
                    scores[i] = ms[i];
                }
            }
            shuffle(rand, scores);
            shuffle(rand, scores);
            let si = 0;
            this.field = new Array(this.H);
            for (let i = 0; i < this.H; i++) {
                this.field[i] = new Array(this.W);
                for (let k = 0; k < this.W; k++) {
                    this.field[i][k] = scores[si];
                    si++;
                }
            }

        } else if (gentype === 1) {

            let scores = new Array(this.H * this.W);
            this.plus = rand.nextInt(p_max - p_min + 1) + p_min;
            this.plus = Math.floor(this.plus * scores.length / 100);
            for (let i = 0; i < scores.length; i++) {
                if (i < this.plus) {
                    scores[i] = rand.nextInt(1000);
                } else {
                    scores[i] = -rand.nextInt(1000);
                }
            }
            shuffle(rand, scores);
            shuffle(rand, scores);
            let si = 0;
            this.field = new Array(this.H);
            for (let i = 0; i < this.H; i++) {
                this.field[i] = new Array(this.W);
                for (let k = 0; k < this.W; k++) {
                    this.field[i][k] = scores[si];
                    si++;
                }
            }

        } else if (gentype === 2) {

            this.plus = 0;
            this.field = new Array(this.H);
            for (let i = 0; i < this.H; i++) {
                this.field[i] = new Array(this.W);
                for (let k = 0; k < this.W; k++) {
                    if ((i+k) % 2 === 0) {
                        this.field[i][k] = -(rand.nextInt(999) + 1);
                    } else {
                        this.field[i][k] = rand.nextInt(999) + 1;
                        this.plus++;
                    }
                }
            }

        } else if (gentype === 3) {

            const scores = new Array(999);
            for (let i = 0; i < 999; i++) {
                scores[i] = i + 1;
            }
            shuffle(rand, scores);
            shuffle(rand, scores);
            let si = 0;
            this.plus = 0;
            this.field = new Array(this.H);
            for (let i = 0; i < this.H; i++) {
                this.field[i] = new Array(this.W);
                for (let k = 0; k < this.W; k++) {
                    if (i % 3 !== 0 || k % 3 !== 0) {
                        this.field[i][k] = -scores[si];
                    } else {
                        this.field[i][k] = scores[si];
                        this.plus++;
                    }
                    si++;
                }
            }
        }
    }
}

function generate(item) {
    const ti = item.i % 70;
    if (ti < 30) {
        testCases[item.i] = new TestCase(item.rand, 10, 10, 0);
    } else if (ti < 60) {
        const p_min = ti - 19;
        const p_max = p_min;
        testCases[item.i] = new TestCase(item.rand, p_min, p_max, 1);
    } else if (ti < 65) {
        testCases[item.i] = new TestCase(item.rand, 0, 0, 2);
    } else if (ti < 70) {
        testCases[item.i] = new TestCase(item.rand, 0, 0, 3);
    }
    item.i++;
    return item;
}

function generateTestCases() {
    const item = {
        i: 0,
        rand: new JavaRandom(6543210n),
    };
    let p = Promise.resolve(item);
    for (let i = 0; i < testCases.length; i++) {
        p.then(generate);
    }
    return p;
}

class JavaRandom {

	constructor(seed) {
		this.setSeed(seed ?? BigInt(Math.floor(Math.random()*1e52)));
	}

	setSeed(seed) {
		this.seed = (seed ^ 0x5DEECE66Dn) & ((1n << 48n) - 1n);
	}

	next(bits) {
		this.seed = (this.seed * 0x5DEECE66Dn + 0xBn) & ((1n << 48n) - 1n);
		return BigInt.asIntN(32, this.seed >> (48n - bits));
	}

	nextInt32() {
        return Number(this.next(32n));
	}

	nextInt(bound) {
		if ((bound & (-bound)) === bound) {
			return Math.floor((this.nextInt32() >>> 1) / ((2**31)/bound));
		}
		let bits;
		let val;
		do {
			bits = this.nextInt32() >>> 1;
			val = bits % bound;
		} while (bits - val + (bound - 1) < 0);
		return val;
	}
}

function drawTestCase(testCase) {

    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    for (let i = 0; i < testCase.H; i++) {
        for (let k = 0; k < testCase.W; k++) {
            const c = 128 + 63 - Math.floor(63 * Math.abs(testCase.field[i][k]) / 1000);
            if (testCase.field[i][k] < 0) {
                ctx.fillStyle = `rgb(255,${c},${c})`;
            } else {
                ctx.fillStyle = `rgb(${c},${c},255)`;
            }
            ctx.fillRect(k * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
        }
    }

    ctx.strokeStyle = 'rgb(127,127,127)';
    ctx.beginPath();
    for (let i = 0; i <= testCase.H; i++) {
        ctx.moveTo(0, i * CELL_SIZE);
        ctx.lineTo(testCase.W * CELL_SIZE, i * CELL_SIZE);
    }
    for (let k = 0; k <= testCase.W; k++) {
        ctx.moveTo(k * CELL_SIZE, 0);
        ctx.lineTo(k * CELL_SIZE, testCase.H * CELL_SIZE);
    }
    ctx.closePath();
    ctx.stroke();
}

function showTestCase(testCase) {
    let s = `${testCase.H} ${testCase.W}\n`;
    for (let i = 0; i < testCase.H; i++) {
        for (let k = 0; k < testCase.W; k++) {
            if (k > 0) s += ' ';
            s += `${testCase.field[i][k]}`;
        }
        s += '\n';
    }
    document.getElementById('testcase').value = s;
}

function setExampleAnswer() {
    const num = Math.max(1, Math.min(testCases.length, Math.floor(
        parseInt(`${document.getElementById('number').value}`))));
    let fileNum = `${num}`;
    while (fileNum.length < 3) { fileNum = '0' + fileNum; }

    const example = document.getElementById('example');
    const answer = document.getElementById('answer');
    const score = document.getElementById('answerscore');

    example.disabled = true;

    fetch(`solution${fileNum}.txt`)
    .then( res => {
        if (res.ok) {
            return res.text();
        } else {
            throw 'error';
        }
    })
    .then( txt => {
        answer.value = txt;
        score.textContent = '';
    })
    .catch( e => {} )
    .finally( () => {
        example.disabled = false;
    });
}

function show() {
    const num = Math.max(1, Math.min(testCases.length, Math.floor(
        parseInt(`${document.getElementById('number').value}`))));

    drawTestCase(testCases[num - 1]);
    showTestCase(testCases[num - 1])

    document.title = `Test Case (${num} / ${testCases.length})`;
    document.querySelector('h1').textContent = `Test Case (${num} /  ${testCases.length})`;

    const chkBtn = document.getElementById('checkbutton');
    chkBtn.value = `${num}`;
    chkBtn.disabled = false;

    const output = document.getElementById('answerscore');
    output.textContent = '';

    const addresult = document.getElementById('addresult');
    addresult.textContent = '';

    const download = document.getElementById('download');
    let fileNum = `${num}`;
    while (fileNum.length < 3) { fileNum = '0' + fileNum; }
    download.href = `testcase${fileNum}.txt`;
    download.download = `testcase${fileNum}.txt`;
    download.hidden = num < 1 || num > DEFAULT_TESTCASE_COUNT;

    const example = document.getElementById('example');
    example.hidden = num < 1 || num > DEFAULT_TESTCASE_COUNT;
}

function drawAnswer(testCase, solution) {

    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    for (let i = 0; i < testCase.H; i++) {
        for (let k = 0; k < testCase.W; k++) {
            if (solution[i].charAt(k) === '1') {
                const c = 63 - Math.floor(63 * Math.abs(testCase.field[i][k]) / 1000);
                if (testCase.field[i][k] < 0) {
                    ctx.fillStyle = `rgb(255,${c},${c})`;
                } else {
                    ctx.fillStyle = `rgb(${c},${c},255)`;
                }
            } else {
                const c = 128 + 63 - Math.floor(63 * Math.abs(testCase.field[i][k]) / 1000);
                if (testCase.field[i][k] < 0) {
                    ctx.fillStyle = `rgb(255,${c},${c})`;
                } else {
                    ctx.fillStyle = `rgb(${c},${c},255)`;
                }
            }
            ctx.fillRect(k * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
        }
    }

    ctx.strokeStyle = 'rgb(127,127,127)';
    ctx.beginPath();
    for (let i = 0; i <= testCase.H; i++) {
        ctx.moveTo(0, i * CELL_SIZE);
        ctx.lineTo(testCase.W * CELL_SIZE, i * CELL_SIZE);
    }
    for (let k = 0; k <= testCase.W; k++) {
        ctx.moveTo(k * CELL_SIZE, 0);
        ctx.lineTo(k * CELL_SIZE, testCase.H * CELL_SIZE);
    }
    ctx.closePath();
    ctx.stroke();

    ctx.strokeStyle = 'rgb(0,0,0)';
    ctx.beginPath();
    for (let i = 0; i < testCase.H; i++) {
        for (let k = 1; k < testCase.W; k++) {
            if (solution[i].charAt(k - 1) === solution[i].charAt(k)) {
                continue;
            }
            ctx.moveTo(k * CELL_SIZE, i * CELL_SIZE);
            ctx.lineTo(k * CELL_SIZE, (i + 1) * CELL_SIZE);
        }
        if (solution[i].charAt(0) === '1') {
            ctx.moveTo(0, i * CELL_SIZE);
            ctx.lineTo(0, (i + 1) * CELL_SIZE);
        }
        if (solution[i].charAt(testCase.W - 1) === '1') {
            ctx.moveTo(testCase.W * CELL_SIZE, i * CELL_SIZE);
            ctx.lineTo(testCase.W * CELL_SIZE, (i + 1) * CELL_SIZE);
        }
    }
    for (let k = 0; k < testCase.W; k++) {
        for (let i = 1; i < testCase.H; i++) {
            if (solution[i - 1].charAt(k) === solution[i].charAt(k)) {
                continue;
            }
            ctx.moveTo(k * CELL_SIZE, i * CELL_SIZE);
            ctx.lineTo((k + 1) * CELL_SIZE, i * CELL_SIZE);
        }
        if (solution[0].charAt(k) === '1') {
            ctx.moveTo(k * CELL_SIZE, 0);
            ctx.lineTo((k + 1) * CELL_SIZE, 0);
        }
        if (solution[testCase.H - 1].charAt(k) === '1') {
            ctx.moveTo(k * CELL_SIZE, testCase.H * CELL_SIZE);
            ctx.lineTo((k + 1) * CELL_SIZE, testCase.H * CELL_SIZE);
        }
    }
    ctx.closePath();
    ctx.stroke();
}

function checkAnswer() {
    const output = document.getElementById('answerscore');
    const num = Math.max(1, Math.min(testCases.length, Math.floor(
        parseInt(document.getElementById('checkbutton').value))));
    const testCase = testCases[num - 1];
    const solution = document.getElementById('answer').value.trim().split(/\r?\n/);
    if (solution.length !== testCase.H) {
        output.textContent = `INVALID (LINECOUNT != ${testCase.H})`;
        return;
    }
    for (let i = 0; i < testCase.H; i++) {
        const s = solution[i].trim();
        if (s.length !== testCase.W || !s.match(/^[01]+$/)) {
            output.textContent = `INVALID (LINE[${i+1}] =~ /^[01]{${testCase.W}}\$/)`;
            return;
        }
        solution[i] = s;
    }
    const visited = new Array(testCase.H);
    for (let i = 0; i < testCase.H; i++) {
        visited[i] = new Array(testCase.W).fill(false);
    }
    const dt = [0, -1, 0, 1, 0];
    let found = false;
    let score = 0;
    for (let i = 0; i < testCase.H; i++) {
        for (let k = 0; k < testCase.W; k++) {
            if (solution[i].charAt(k) === '0') {
                continue;
            }
            if (visited[i][k]) {
                continue;
            }
            if (found) {
                output.textContent = 'INVALID (UNCONNECTED)';
                return;
            }
            found = true;
            const stack = [[i, k]];
            visited[i][k] = true;
            while (stack.length > 0) {
                const p = stack.pop();
                score += testCase.field[p[0]][p[1]];
                for (let d = 0; d < 4; d++) {
                    const y = p[0] + dt[d];
                    const x = p[1] + dt[d + 1];
                    if (y<0||x<0||y>=testCase.H||x>=testCase.W) {
                        continue;
                    }
                    if (visited[y][x]) {
                        continue;
                    }
                    if (solution[y].charAt(x) === '0') {
                        continue;
                    }
                    visited[y][x] = true;
                    stack.push([y, x]);
                }
            }
        }
    }
    if (!found) {
        output.textContent = 'INVALID (UNCONNECTED)';
        return;
    }
    output.textContent = `SCORE = ${score}`;
    drawAnswer(testCase, solution);
}

function addTestCase() {
    const output = document.getElementById('addresult');
    const source = document.getElementById('testcase').value.trim().split(/\r?\n/);
    if (source.length < 2) {
        output.textContent = 'INVALID FORMAT';
        return;
    }
    const firstLine = source[0].trim().split(/\s+/).map( s => parseInt(s) );
    if (firstLine.length != 2) {
        output.textContent = 'INVALID FORMAT';
        return;
    }
    if (firstLine.some((v => isNaN(v) || v < 2 || v > 30))) {
        output.textContent = 'INVALID FORMAT';
        return;
    }
    const H = firstLine[0];
    const W = firstLine[1];
    if (source.length != H + 1) {
        output.textContent = 'INVALID FORMAT';
        return;
    }
    const field = new Array(H);
    for (let i = 0; i < H; i++) {
        field[i] = source[i + 1].trim().split(/\s+/).map( s => parseInt(s) );
        if (field[i].some( v => isNaN(v) || v < -1000 || v > 1000 )) {
            output.textContent = 'INVALID FORMAT';
            return;
        }
    }
    testCases.push({
        H: H,
        W: W,
        field: field,
    });
    const num = document.getElementById('number');
    num.max = testCases.length;
    num.value = num.max;
    output.textContent = `ADD TEST CASE No.${testCases.length}`;
}

function loadTestCaseFile() {
    const addFile = document.getElementById('addfile');
    if (addFile.files.length !== 1) {
        return;
    }
    for (const file of addFile.files) {
        file.text()
        .then( t => {
            document.getElementById('testcase').value = t;
        })
        .catch( () => {
            document.getElementById('testcase').value = 'failed to load file';
        });
    }
}

function loadAnswerFile() {
    const chkFile = document.getElementById('checkfile');
    if (chkFile.files.length !== 1) {
        return;
    }
    for (const file of chkFile.files) {
        file.text()
        .then( t => {
            document.getElementById('answer').value = t;
        })
        .catch( () => {
            document.getElementById('answer').value = 'failed to load file';
        });
    }
}

window.addEventListener('load', () => {
    generateTestCases()
    .then( e => {

        const showBtn = document.getElementById('selectbutton');
        showBtn.addEventListener('click', show);
        showBtn.disabled = false;

        const chkBtn = document.getElementById('checkbutton');
        chkBtn.addEventListener('click', checkAnswer);

        const addBtn = document.getElementById('addbutton');
        addBtn.addEventListener('click', addTestCase);
        addBtn.disabled = false;

        const addFile = document.getElementById('addfile');
        addFile.addEventListener('change', loadTestCaseFile);
        addFile.disabled = false;

        const chkFile = document.getElementById('checkfile');
        chkFile.addEventListener('change', loadAnswerFile);
        chkFile.disabled = false;

        const example = document.getElementById('example');
        example.addEventListener('click', setExampleAnswer);

        const clrSrc = document.getElementById('clearsource');
        clrSrc.addEventListener('click', () => {
            document.getElementById('testcase').value = '';
        });

        const clrAns = document.getElementById('clearanswer');
        clrAns.addEventListener('click', () => {
            document.getElementById('answer').value = '';
        });
    });
});

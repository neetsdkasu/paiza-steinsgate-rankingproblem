'use strict';

const INF = 100000000;
const DT = [0, -1, 0, 1, 0];

const CYCLE_LIMIT = 80;
const KICKS = 4;
const ALPHA = 0.05;
const SCHEDULE = 4;
const SCALE = 1;

function shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const k = Math.floor(Math.random() * (i + 1));
        const tmp = arr[k];
        arr[k] = arr[i];
        arr[i] = tmp;
    }
}

function info(msg) {
    document.getElementById('solverinfo').textContent = msg;
}

// ※このソルバーはExample生成のソルバーとは異なります
class Solver {
    constructor() {
        this.timeoutID = 0;
        this.state = 0;
        this.testCase = null;
        this.best = {
            score: 0,
            solution: null,
            updated: false,
        };
        this.kicks = 0;
        this.sa = {
            cycle: 0,
            sum: 0,
            count: 0,
        };
        this.plusGroup = {
            count: 0,
            map: null,
            scores: null,
        };
        this.backupGroup = {
            count: 0,
            map: null,
            scores: null,
        };
        this.dijkstraState = {
            order: null,
            orderIndex: 0,
            connectCount: 0,
            group: {
                count: 0,
                map: null,
                scores: null,
            },
        };
        this.disconnectState = {
            count: 0,
            success: 0,
            list: null,
            connectMap: null,
            group: {
                count: 0,
                map: null,
                scores: null,
            },
        };
    }

    set(testCase) {
        this.testCase = testCase;
        this.state = 0;
        this.best.score = 0;
        const s = '0'.repeat(testCase.W);
        this.best.solution = new Array(testCase.H).fill(s);
        this.best.updated = false;
        this.sa.cycle = 0;
        this.sa.sum = 0;
        this.sa.count = 0;
        this.kicks = KICKS;
        info('');
    }

    start() {
        document.querySelectorAll('button, input, textarea')
            .forEach( b => { b.disabled = b.id !== 'solverstop' && b.id != 'solverspeed'; });
        this.timeoutID = window.setTimeout(timeoutFunc);
        this.showBestAnswer();
    }

    stop() {
        if (this.timeoutID !== 0) {
            window.clearTimeout(this.timeoutID);
            this.timeoutID = 0;
        }
        document.querySelectorAll('button, input, textarea')
            .forEach( b => { b.disabled = b.id === 'solverstop'; });
    }

    contains(y, x) {
        return y >= 0 && x >= 0 && y < this.testCase.H && x < this.testCase.W;
    }

    drawBestAnswer() {
        drawAnswer(this.testCase, this.best.solution);
    }

    showBestAnswer() {
        document.getElementById('answerscore').textContent = `SCORE = ${this.best.score}`;
        document.getElementById('answer').value = this.best.solution.join('\n');
    }

    updateBest(group) {
        let id = 1;
        for (let g = 2; g <= group.count; g++) {
            if (group.scores[g] > group.scores[id]) {
                id = g;
            }
        }
        if (group.scores[id] <= this.best.score) {
            return;
        }
        this.best.updated = true;
        this.best.score = group.scores[id];
        const TC = this.testCase;
        for (let y = 0; y < TC.H; y++) {
            let s = '';
            for (let x = 0; x < TC.W; x++) {
                if (group.map[y][x] === id) {
                    s += '1';
                } else {
                    s += '0';
                }
            }
            this.best.solution[y] = s;
        }
    }

    drawGroups(group) {
        const TC = this.testCase;
        const solution = new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            let s = '';
            for (let x = 0; x < TC.W; x++) {
                if (group.map[y][x] === 0) {
                    s += '0';
                } else {
                    s += '1';
                }
            }
            solution[y] = s;
        }
        drawAnswer(TC, solution);
    }

    solve() {
        switch (this.state) {
            case 0:
                this.findPlusGrpous();
                this.backup(this.plusGroup);
                this.initDijkstra(this.plusGroup);
                this.drawGroups(this.plusGroup);
                this.state = 1;
                break;
            case 1:
                if (this.processDijkstra(false)) {
                    this.drawGroups(this.dijkstraState.group);
                } else {
                    this.initDisconnect(this.dijkstraState.group);
                    this.state = 2;
                    this.drawGroups(this.disconnectState.group);
                }
                break;
            case 2:
                if (!this.processDisconnect()) {
                    this.initDijkstra(this.disconnectState.group, this.disconnectState.success);
                    this.state = 3;
                }
                this.drawGroups(this.disconnectState.group);
                break;
            case 3:
                if (!this.processDijkstra(true)) {
                    this.sa.cycle++;
                    info(`CYCLE: ${this.sa.cycle} / ${CYCLE_LIMIT}`);
                    if (this.sa.cycle < CYCLE_LIMIT) {
                        this.state = 1;
                    } else {
                        this.cleanupBest();
                        this.kicks--;
                        if (this.kicks > 0) {
                            this.initKick();
                            info(`KICK: ${KICKS - this.kicks} / ${KICKS}`);
                            this.state = 1;
                        } else {
                            this.state = 4;
                        }
                    }
                }
                this.drawGroups(this.dijkstraState.group);
                break;
            default:
                this.showBestAnswer();
                this.drawBestAnswer();
                info(`RESULT: SCORE = ${this.best.score}`);
                this.stop();
                break;
        }
        if (this.best.updated) {
            this.best.updated = false;
            this.showBestAnswer();
        }
    }

    backup(group) {
        const TC = this.testCase;
        this.backupGroup.map ??= new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            this.backupGroup.map[y] = group.map[y].slice();
        }
        this.backupGroup.count = group.count;
        this.backupGroup.scores = group.scores.slice();
    }

    initKick() {
        const TC = this.testCase;
        const map = new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            map[y] = this.plusGroup.map[y].slice();
            for (let x = 0; x < TC.W; x++) {
                map[y][x] += parseInt(this.best.solution[y].charAt(x));
            }
        }
        const group = {
            count: 0,
            map: map,
            scores: null,
        };
        this.regroup(group);
        this.backup(group);
        this.initDijkstra(group);
        this.sa.cycle = 0;
        this.sa.sum = 0;
        this.sa.count = 0;
    }

    cleanupBest() {
        const TC = this.testCase;
        let map = new Array(TC.H);
        let score = this.best.score;
        const list = [];
        for (let y = 0; y < TC.H; y++) {
            map[y] = new Array(TC.W);
            for (let x = 0; x < TC.W; x++) {
                map[y][x] = parseInt(this.best.solution[y].charAt(x));
                if (map[y][x] !== 0 && TC.field[y][x] < 0) {
                    list.push([y, x]);
                }
            }
        }
        shuffleArray(list);
        for (const pos of list) {
            if (map[pos[0]][pos[1]] === 0) { continue; }
            const groupList = [];
            let changed = false;
            map[pos[0]][pos[1]] = 0;
            for (let d = 0; d < 4; d++) {
                const yy = pos[0] + DT[d];
                const xx = pos[1] + DT[d+1];
                if (!this.contains(yy, xx)) { continue; }
                if (map[yy][xx] === 0) { continue; }
                let found = false;
                for (const group of groupList) {
                    if (group.map[yy][xx] !== 0) {
                        found = true;
                        break;
                    }
                }
                if (found) { continue; }
                const tmpGroup = {
                    map: new Array(TC.H),
                    score: 0,
                };
                for (let z = 0; z < TC.H; z++) {
                    tmpGroup.map[z] = new Array(TC.W).fill(0);
                }
                this.traceGroup(map, tmpGroup, yy, xx);
                if (tmpGroup.score > score) {
                    score = tmpGroup.score;
                    map = tmpGroup.map;
                    changed = true;
                    break;
                }
                groupList.push(tmpGroup);
            }
            if (!changed) {
                map[pos[0]][pos[1]] = 1;
            }
        }
        if (score > this.best.score) {
            this.best.updated = true;
            this.best.score = score;
            for (let y = 0; y < TC.H; y++) {
                let s = '';
                for (let x = 0; x < TC.W; x++) {
                    s += `${map[y][x]}`;
                }
                this.best.solution[y] = s;
            }
            info('CLEANUP');
        }
    }

    traceGroup(srcMap, dstGroup, y, x) {
        dstGroup.map[y][x] = 1;
        dstGroup.score += this.testCase.field[y][x];
        for (let d = 0; d < 4; d++) {
            const yy = y + DT[d];
            const xx = x + DT[d + 1];
            if (!this.contains(yy, xx)) { continue; }
            if (srcMap[yy][xx] === 0) { continue; }
            if (dstGroup.map[yy][xx] !== 0) { continue; }
            this.traceGroup(srcMap, dstGroup, yy, xx);
        }
    }

    regroup(group) {
        const TC = this.testCase;
        let id = 0;
        let scores = [0];
        let map = new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            map[y] = new Array(TC.W).fill(0);
        }
        const stack = [];
        for (let y = 0; y < TC.H; y++) {
            for (let x = 0; x < TC.W; x++) {
                if (group.map[y][x] === 0) { continue; }
                if (map[y][x] !== 0) { continue; }
                id++;
                map[y][x] = id;
                scores.push(TC.field[y][x]);
                stack.push([y, x]);
                while (stack.length > 0) {
                    const pos = stack.pop();
                    for (let d = 0; d < 4; d++) {
                        const yy = pos[0] + DT[d];
                        const xx = pos[1] + DT[d + 1];
                        if (!this.contains(yy, xx)) { continue; }
                        if (group.map[yy][xx] === 0) { continue; }
                        if (map[yy][xx] !== 0) { continue; }
                        map[yy][xx] = id;
                        scores[id] += TC.field[yy][xx];
                        stack.push([yy, xx]);
                    }
                }
            }
        }
        group.count = id;
        group.map = map;
        group.scores = scores;
    }

    findPlusGrpous() {
        const TC = this.testCase;
        let id = 0;
        let scores = [0];
        let map = new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            map[y] = new Array(TC.W).fill(0);
        }
        const stack = [];
        for (let y = 0; y < TC.H; y++) {
            for (let x = 0; x < TC.W; x++) {
                if (TC.field[y][x] < 0) { continue; }
                if (map[y][x] !== 0) { continue; }
                id++;
                map[y][x] = id;
                scores.push(TC.field[y][x]);
                stack.push([y, x]);
                while (stack.length > 0) {
                    const pos = stack.pop();
                    for (let d = 0; d < 4; d++) {
                        const yy = pos[0] + DT[d];
                        const xx = pos[1] + DT[d + 1];
                        if (!this.contains(yy, xx)) { continue; }
                        if (TC.field[yy][xx] < 0) { continue; }
                        if (map[yy][xx] !== 0) { continue; }
                        map[yy][xx] = id;
                        scores[id] += TC.field[yy][xx];
                        stack.push([yy, xx]);
                    }
                }
            }
        }
        this.plusGroup.count = id;
        this.plusGroup.map = map;
        this.plusGroup.scores = scores;
    }

    initDijkstra(group, connectCount) {
        const TC = this.testCase;
        const order = new Array(group.count);
        for (let i = 0; i < group.count; i++) {
            order[i] = i + 1;
        }
        shuffleArray(order);
        const map = new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            map[y] = group.map[y].slice();
        }
        connectCount ??= 0;
        if (connectCount === 0) {
            connectCount = Math.floor(Math.random() * ((group.count >> 1) + 1))
                            + Math.floor(group.count / 5) + 1;
        } else {
            connectCount = Math.floor(Math.random() * connectCount);
        }

        this.dijkstraState.order = order;
        this.dijkstraState.orderIndex = 0;
        this.dijkstraState.connectCount = connectCount;
        this.dijkstraState.group.count = group.count;
        this.dijkstraState.group.map = map;
        this.dijkstraState.group.scores = group.scores.slice();
    }

    processDijkstra(wildly) {
        if (wildly) {
            if (this.dijkstraState.connectCount <= 0) {
                return false;
            }
        }
        const TC = this.testCase;
        const group = this.dijkstraState.group;
        while (true) {
            if (this.dijkstraState.orderIndex >= this.dijkstraState.order.length) {
                return false;
            }
            const groupId = this.dijkstraState.order[this.dijkstraState.orderIndex];
            if (group.scores[groupId] === -INF) {
                this.dijkstraState.orderIndex++;
                continue;
            }
            const map = new Array(TC.H);
            const parent = new Array(TC.H);
            const visited = new Array(TC.H);
            for (let y = 0; y < TC.H; y++) {
                map[y] = new Array(TC.W).fill(-INF);
                parent[y] = new Array(TC.W).fill(null);
                visited[y] = new Array(TC.H).fill(false);
            }
            for (let y = 0; y < TC.H; y++) {
                for (let x = 0; x < TC.W; x++) {
                    if (group.map[y][x] !== groupId) { continue; }
                    for (let d = 0; d < 4; d++) {
                        const yy = y + DT[d];
                        const xx = x + DT[d + 1];
                        if (!this.contains(yy, xx)) { continue; }
                        if (group.map[yy][xx] !== 0) { continue; }
                        map[yy][xx] = TC.field[yy][xx];
                    }
                }
            }
            let lowestCost = 0;
            for (let g = 1; g <= group.count; g++) {
                if (g !== groupId) {
                    lowestCost = Math.max(lowestCost, group.scores[g]);
                }
            }
            if (wildly) {
                const factor = ((KICKS - this.kicks) >> 1) + 3;
                const fluctuation = Math.floor(Math.random() * factor) + 1;
                lowestCost = -Math.min(lowestCost, group.scores[groupId]) * fluctuation;
            } else {
                lowestCost = -Math.min(lowestCost, group.scores[groupId]);
            }
            const list = [];
            while (true) {
                let sel = {
                    score: -INF,
                    y: 0,
                    x: 0,
                    r: 0,
                };
                for (let y = 0; y < TC.H; y++) {
                    for (let x = 0; x < TC.W; x++) {
                        if (map[y][x] === -INF) { continue; }
                        if (visited[y][x]) { continue; }
                        if (map[y][x] <= lowestCost) { continue; }
                        if (map[y][x] > sel.score) {
                            sel.score = map[y][x];
                            sel.y = y;
                            sel.x = x;
                            sel.r = Math.random();
                        } else if (map[y][x] === sel.score) {
                            const r = Math.random();
                            if (r > sel.r) {
                                sel.y = y;
                                sel.x = x;
                                sel.r = r;
                            }
                        }
                    }
                }
                if (sel.score === -INF) {
                    this.dijkstraState.orderIndex++;
                    break;
                }
                visited[sel.y][sel.x] = true;
                let found = false;
                for (let d = 0; d < 4; d++) {
                    const yy = sel.y + DT[d];
                    const xx = sel.x + DT[d + 1];
                    if (!this.contains(yy, xx)) { continue; }
                    if (group.map[yy][xx] !== 0) {
                        if (group.map[yy][xx] === groupId) {
                            continue;
                        }
                        found = true;
                        break;
                    }
                    if (visited[yy][xx]) { continue; }
                    const cost = TC.field[yy][xx] + sel.score;
                    if (cost <= lowestCost) { continue; }
                    if (cost <= map[yy][xx]) { continue; }
                    map[yy][xx] = cost;
                    parent[yy][xx] = [sel.y, sel.x];
                }
                if (!found) { continue; }
                if (wildly) {
                    list.push(sel);
                    if (list.length < 4) { continue; }
                } else {
                    const gs = [];
                    let tmpPos = [sel.y, sel.x];
                    let tmpScore = sel.score;
                    while (tmpPos !== null) {
                        for (let d = 0; d < 4; d++) {
                            const yy = tmpPos[0] + DT[d];
                            const xx = tmpPos[1] + DT[d + 1];
                            if (!this.contains(yy, xx)) { continue; }
                            const g = group.map[yy][xx];
                            if (g === 0 || g === groupId) { continue; }
                            if (gs.includes(g)) continue;
                            gs.push(g);
                            tmpScore += group.scores[g];
                        }
                        tmpPos = parent[tmpPos[0]][tmpPos[1]];
                    }
                    if (tmpScore <= 0) { continue; }
                    list.push(sel);
                }
                sel = list[Math.floor(Math.random() * list.length)];
                let pos = [sel.y, sel.x];
                while (pos !== null) {
                    for (let d = 0; d < 4; d++) {
                        const yy = pos[0] + DT[d];
                        const xx = pos[1] + DT[d + 1];
                        if (!this.contains(yy, xx)) { continue; }
                        const g = group.map[yy][xx];
                        if (g === 0 || g === groupId) { continue; }
                        this.changeGroup(group, g, groupId, yy, xx);
                        group.scores[groupId] += group.scores[g];
                        group.scores[g] = -INF;
                    }
                    group.map[pos[0]][pos[1]] = groupId;
                    group.scores[groupId] += TC.field[pos[0]][pos[1]];
                    pos = parent[pos[0]][pos[1]];
                }
                info(`make a path from Group No.${groupId}`);
                this.dijkstraState.orderIndex++;
                this.updateBest(group);
                if (wildly) {
                    this.disconnectState.connectCount--;
                }
                return true;
            }
        }
    }

    changeGroup(group, fromId, toId, y, x) {
        group.map[y][x] = toId;
        for (let d = 0; d < 4; d++) {
            const yy = y + DT[d];
            const xx = x + DT[d + 1];
            if (!this.contains(yy, xx)) { continue; }
            if (group.map[yy][xx] !== fromId) { continue; }
            this.changeGroup(group, fromId, toId, yy, xx);
        }
    }

    accept(group) {
        const curScore = group.scores.reduce( (acc, v) => Math.max(acc, v) );
        const backupScore = this.backupGroup.scores.reduce( (acc, v) => Math.max(acc, v) );
        if (curScore >= backupScore) {
            info('ACCEPTED');
            this.backup(group);
            return group;
        }
        this.sa.sum += backupScore - curScore;
        this.sa.count++;
        const mean = SCALE * this.sa.sum / this.sa.count;
        const t = Math.pow(ALPHA, Math.pow(this.sa.cycle / this.sa.limit, SCHEDULE));
        const v = Math.log10(backupScore - curScore) / Math.log10(mean);
        const p = Math.exp(-v / t);
        if (Math.random() < p) {
            info('ACCEPTED');
            this.backup(group);
            return group;
        } else {
            info('REJECTED');
            return this.backupGroup;
        }
    }

    initDisconnect(group) {
        group = this.accept(group);
        const TC = this.testCase;
        const map = new Array(TC.H);
        const connectMap = new Array(TC.H);
        for (let y = 0; y < TC.H; y++) {
            map[y] = group.map[y].slice();
            connectMap[y] = new Array(TC.W).fill(0);
        }
        const list = [];
        for (let y = 0; y < TC.H; y++) {
            for (let x = 0; x < TC.W; x++) {
                if (map[y][x] === 0) { continue; }
                if (TC.field[y][x] >= 0) { continue; }
                let c = 0;
                for (let d = 0; d < 4; d++) {
                    const yy = y + DT[d];
                    const xx = x + DT[d + 1];
                    if (!this.contains(yy, xx)) { continue; }
                    if (map[yy][xx] === 0) { continue; }
                    if (TC.field[yy][xx] >= 0) { continue; }
                    c++;
                }
                connectMap[y][x] = c;
                list.push([y, x]);
            }
        }
        shuffleArray(list);
        this.disconnectState.count = Math.floor(Math.random() * 4) + 1;
        this.disconnectState.success = 0;
        this.disconnectState.list = list;
        this.disconnectState.connectMap = connectMap;
        this.disconnectState.group.count = group.count;
        this.disconnectState.group.map = map;
        this.disconnectState.group.scores = group.scores.slice();
    }

    processDisconnect() {
        if (this.disconnectState.count === 0) {
            return false;
        }
        const list = this.disconnectState.list;
        const group = this.disconnectState.group;
        let pos = null;
        for (let i = 0; i < list.length; i++) {
            const tmp = list[i];
            if (group.map[tmp[0]][tmp[1]] !== 0) {
                pos = tmp;
                break;
            }
        }
        if (pos === null) {
            return false;
        }
        const TC = this.testCase;
        const connectMap = this.disconnectState.connectMap;
        const stack = [pos];
        info(`disjoint Group No.${group.map[pos[0]][pos[1]]}`);
        group.map[pos[0]][pos[1]] = 0;
        while (stack.length > 0) {
            pos = stack.pop();
            for (let d = 0; d < 4; d++) {
                const yy = pos[0] + DT[d];
                const xx = pos[1] + DT[d + 1];
                if (!this.contains(yy, xx)) { continue; }
                if (group.map[yy][xx] === 0) { continue; }
                if (TC.field[yy][xx] >= 0) { continue; }
                if (connectMap[yy][xx] > 2) {
                    connectMap[yy][xx]--;
                    continue;
                }
                group.map[yy][xx] = 0;
                stack.push([yy, xx]);
            }
        }
        this.disconnectState.count--;
        this.disconnectState.success++;
        this.regroup(group);
        this.updateBest(group);
        return true;
    }
}

const solver = new Solver();

function timeoutFunc() {
    solver.solve();
    if (solver.timeoutID !== 0) {
        window.clearTimeout(this.timeoutID);
        const speed = parseInt(document.getElementById('solverspeed').value);
        solver.timeoutID = window.setTimeout(timeoutFunc, speed);
    }
}

window.addEventListener('load', () => {
    const param = new URLSearchParams(window.location.search).get('solver');
    if (param === null || param === undefined) {
        return;
    }
    const main = document.querySelector('main');
    const first = main.firstChild;
    main.insertBefore(document.createElement('hr'), first);
    const section = main.insertBefore(document.createElement('section'), first);
    section.appendChild(document.createElement('h4')).textContent = 'Solver';
    const div = section.appendChild(document.createElement('div'));
    div.classList.add('solver');
    const div2 = section.appendChild(document.createElement('div'));
    const speed = div.appendChild(document.createElement('input'));
    speed.id = 'solverspeed';
    speed.type = 'number';
    speed.min = 0;
    speed.max = 10000;
    speed.value = 50;
    speed.title = 'animation interval (msec)';
    const slvBtn = div.appendChild(document.createElement('button'));
    slvBtn.id = 'solerstart';
    slvBtn.textContent = 'Solve';
    slvBtn.disabled = true;
    slvBtn.addEventListener('click', () => solver.start() );
    const stpBtn = div.appendChild(document.createElement('button'));
    stpBtn.id = 'solverstop';
    stpBtn.textContent = 'Stop';
    stpBtn.disabled = true;
    stpBtn.addEventListener('click', () => solver.stop() );
    const output = div2.appendChild(document.createElement('output'));
    output.id = 'solverinfo';
    const selBtn = document.getElementById('selectbutton');
    selBtn.addEventListener('click', () => {
        const num = Math.max(1, Math.min(testCases.length, Math.floor(
            parseInt(`${document.getElementById('number').value}`))));
        solver.set(testCases[num - 1]);
        if (slvBtn.disabled) {
            slvBtn.disabled = false;
        }
    });
});